﻿using UnityEngine;
using System.Collections;

public class Gracz : MonoBehaviour
{
    public int Zycie = 100;
    public int atak = 10;

    public RaycastHit hit;
    public float zasieg = 1.5f;
    public float dystans;
    private Transform transformGracza;

    // Use this for initialization
    void Start()
    {
        transformGracza = gameObject.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            if (Physics.Raycast(transformGracza.position, transformGracza.TransformDirection(Vector3.forward), out hit))
            {
                dystans = hit.distance;
                if (dystans < zasieg)
                {
                    hit.transform.SendMessage("ZmienZycie", atak, SendMessageOptions.DontRequireReceiver);
                }
            }
        }
    }
}
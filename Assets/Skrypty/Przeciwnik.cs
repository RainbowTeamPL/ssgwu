﻿using System.Collections;
using UnityEngine;

public class Przeciwnik : MonoBehaviour
{
    public int Zycie = 100;

    public void ZmienZycie(int ilosc)
    {
        Zycie = Zycie - ilosc;
    }

    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        if (Zycie <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}